import java.time.LocalDate;
import java.util.Arrays;

public class Customer {

	private static int clientCounter;
	private Integer clientId;
	private String taxId;
	private LocalDate Dob;
	private Account account;
	private Account[] customerAccountsList = new Account[3];
// editable:
	private String password;
	private String name;
	private Double balance;
	private String profession;
	private String email;
	private String phone;
	private String mobilePhone;
	
	protected Customer() {
	}
// Constructor para o fillDB()
	protected Customer(String name, String password, String taxId,  String email) {
		this.taxId = taxId;
		this.password = password;
		this.name = name;
		this.email = email;
		calculateClientId();
	}
	
	protected Account[] getCustomerAccountsList() {
		return customerAccountsList;
	}
	protected void setCustomerAccountsList(Account[] customerAccountsList) {
		this.customerAccountsList = customerAccountsList;
	}
	protected Account getAccount() {
		return account;
	}
	protected void setAccount(Account account) {
		this.account = account;
	}
	protected int setClientId(int clientId) {
		this.clientId = clientId;
		return clientId;
	}
	protected void calculateClientId() {
		clientCounter++;
		setClientId(clientCounter);
	}
	protected String getTaxId() {
		return taxId;
	}
	protected void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	protected String getPassword() {
		return password;
	}
	protected void setPassword(String password) {
		this.password = password;
	}
	protected String getName() {
		return name;
	}
	protected void setName(String name) {
		this.name = name;
	}

	protected String getEmail() {
		return email;
	}
	protected void setEmail(String email) {
		this.email = email;
	}
	protected void setDob(LocalDate dob) {
		this.Dob = dob;
	}
	protected Double getBalance() {
		return balance;
	}
	protected void setBalance(Double balance) {
		this.balance = balance;
	}
//secondary setters and getters	
	protected String getProfession() {
		return profession;
	}
	protected void setProfession(String profession) {
		this.profession = profession;
	}
	protected String getPhone() {
		return phone;
	}
	protected void setPhone(String phone) {
		this.phone = phone;
	}
	protected String getMobilePhone() {
		return mobilePhone;
	}
	protected void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	@Override
	public String toString() {
		return "Customer [clientId=" + clientId + ", taxId=" + taxId + ", Name=" + name + "]";
	}

	
	
	
}
