import java.time.LocalDate;
import java.util.Calendar;
import java.util.Scanner;

public class Program {
	private static Customer[] customers = new Customer[8];
	private static Scanner sc = new Scanner(System.in);
	private static Integer input;
	private static int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	private static Customer loggedUser = null;
	private static boolean isLogged;
	private static boolean isCorrectAccountId;
	
	public static void main(String[] args) {

		System.out.println("\u001b[44mWelcome to Rumos Digital Bank");
		System.out.println("\u001b[0m");
		fillDB();
		menuHome();
	}
		private static void menuHome() {
			
			do {
				System.out.printf("Please choose from the menu%n"
						+ "1 - Manage%n"
						+ "2 - Finances%n"
						+ "3 - ATM%n"
						+ "0 - Quit%n");
				
				input = sc.nextInt();
					switch (input) {
	
					case 0:
						System.out.println("Best regards from RDB");
						break;
					case 1:
						menuManage();
						break;
					case 2:
						System.out.printf("Please Log in%n");
						login();
						menuFinances();
						break;
					case 3:
						// ATM
					default:
						System.err.println("Invalid Option");
						break; 
						}
			} while (input != 0);
		}

		private static void menuManage() {
			
			do {
				System.out.printf("0 - Exit%n"
						+ "1 - Create customer%n"
						+ "2 - Show customer by name%n"
						+ "3 - Show customer by Tax ID%n"
						+ "4 - Show all customers%n"
						+ "5 - Edit customer by Tax ID%n"
						+ "6 - Delete customer by Tax ID%n"
						+ "7 - Show customer accounts by Tax ID%n"
						+ "8 - Show all accounts%n");

				input = sc.nextInt();

				switch (input) {
				case 0:
					menuHome();
					break;
				case 1:
					createNewCustomer();
					break;
				case 2:
					showCustomerByName();
					break;
				case 3:
					showCustomerByTaxId();
					break;
				case 4:
					showAllCustomers();
					break;
				case 5:
					editCustomerByTaxId();
					break;
				case 6:
					deleteCustomerByTaxId();
					break;
				case 7:
					showAccountsByTaxId();
					break;
				case 8:
					showAllAccounts();
					break;
				default:
					break;
				}
			} while (input != 0);
			
		}
// check if customers array has empty space to add new customer
		private static void createNewCustomer() {
			
			for (int i=0;i<customers.length; i++) {
				if (customers[i] == null) {
					customers[i] = addCustomer(); 
					System.out.println("Customer added");
					return;
				}
			}
			System.err.println("Database is full");
		}
	
		private static Customer addCustomer() {
			
			Customer newCustomer = new Customer();
			newCustomer.calculateClientId();
			
			System.out.print("Insert name: ");
			newCustomer.setName(sc.next().strip());
			
			System.out.print("Insert password: ");
			newCustomer.setPassword(sc.next().strip());
			
//set Tax ID // check if duplicated
			System.out.print("Insert taxId: ");
			String taxId = sc.next().strip();
			for (int i=0;i<customers.length;i++) {
				if(customers[i] == null) continue;
				if(customers[i].getTaxId().equals(taxId))
					throw new IllegalArgumentException("Tax ID already exists");
			}
			newCustomer.setTaxId(taxId);
			
			System.out.print("Insert email: ");
			newCustomer.setEmail(sc.next().strip());
			
			System.out.print("Insert day of birth: ");
			Integer day = sc.nextInt();
			
			System.out.print("Insert month of birth: ");
			Integer month = sc.nextInt();

			System.out.print("Insert year of birth: ");
			Integer year = sc.nextInt();
			if (currentYear-year>=18) {
				newCustomer.setDob(LocalDate.of(year, month, day));
			}
			else
				throw new IllegalArgumentException("You must be at leat 18 years old to open an account.");
			
			System.out.print("Enter balance: ");
			Double balance = sc.nextDouble();
			if (balance >= 50)
				newCustomer.setBalance(balance);
			else
				throw new IllegalArgumentException("Initial deposit must be at least 50�.");
			
// Setting new Account to new Customer			
			Account newAccount = new Account(newCustomer,balance);
			newCustomer.setAccount(newAccount);
			
//adding first acc to Account List			
			newCustomer.getCustomerAccountsList()[0] = newAccount;
			
//adding DebitCard and Credit Card to Account
			newAccount.setDebitCard(createDebitCard());
			newAccount.setCreditCard(createCreditCard());
			
			return newCustomer;
		}

		private static void showCustomerByName() {
			
			System.out.print("Insert name: ");
			String searchName = sc.next().strip();
			
			//checkCustomerListHasNullSlot()
			for (int i=0; i<customers.length; i++) {
				if (customers[i]==null) continue;
				if (searchName.equals(customers[i].getName())) {
					System.out.println(customers[i]);
					return;
				}	
			}
			System.err.println("Name not found");
		}
		
		private static void showCustomerByTaxId() {
			System.out.print("Insert tax ID: ");
			String searchTaxId = sc.next().strip();
			// checkCustomerListHasNullSlot()
			for (int i=0; i<customers.length; i++) {
				if (customers[i]==null)	
					continue;
				if(searchTaxId.equals(customers[i].getTaxId())) {
					System.out.println(customers[i]);
					return;
				}
			}
			System.err.println("Tax ID not found");
		}
		
		private static void showAllCustomers() {
			for (int i = 0; i < customers.length; i++) {
				if (customers[i] == null) continue;
				System.out.println(customers[i]);
			}
		}
		
		private static void editCustomerByTaxId() {
			
			Customer c = chooseCustomerByTaxId();
			
			do {
				System.out.printf("What do you want to edit?%n"
								+ "1 - Name%n"
								+ "2 - Password%n"
								+ "3 - Email%n"
								+ "4 - Profession%n"
								+ "5 - Phone%n"
								+ "6 - Cell phone%n"
								+ "0 - Exit%n");
				
				input = sc.nextInt();
				for(int i=0;i<customers.length;i++) {
					
				}
				switch(input) {
					
				case 1:
					c.setName(inputEditCustomer());
					break;
				case 2:
					c.setPassword(inputEditCustomer());
					break;
				case 3:
					c.setPassword(inputEditCustomer());
					break;
				case 4:
					c.setProfession(inputEditCustomer());
					break;
				case 5:
					c.setPhone(inputEditCustomer());
					break;
				case 6:
					c.setMobilePhone(inputEditCustomer());
					break;
				case 0:
					menuManage();
					break;
				}
				
		} while(input!=0);
	}
		private static Customer chooseCustomerByTaxId() {
			Customer c = null;
			System.out.print("Insert tax ID: ");
			String searchTaxId = sc.next().strip();
			// checkCustomerListHasNullSlot()
			for (int i=0; i<customers.length; i++) {
				if (customers[i]==null)	continue;
				if(searchTaxId.equals(customers[i].getTaxId())) {
					c = customers[i];
				
				}
		}
			return c;
}	
		private static String inputEditCustomer() {
			System.out.print("Edit field: ");
			String edit = sc.next();
			return edit;
		}
		
		private static void deleteCustomerByTaxId() {
		
			System.out.println("Insert Tax ID to be deleted");
			String deleteTaxId = sc.next();
			
			for (int i=0; i<customers.length; i++) {
				if (customers[i]==null)	
					continue;
				if(deleteTaxId.equals(customers[i].getTaxId())) {
					customers[i] = null;
					return;
				}
			}
			System.err.println("Tax ID not found");
		}
		
		private static void showAccountsByTaxId() {
			System.out.print("Insert Tax ID: ");
			String taxId = sc.next();
			for(int i=0;i<customers.length;i++) {
				if (customers[i]==null)	
					continue;
				if(customers[i].getTaxId().equals(taxId)) {
					System.out.println(customers[i].getAccount());
					return;
				}
			}
			System.err.println("Tax ID not found");
		}
		
		private static void showAllAccounts() {
			for (int i=0;i<customers.length;i++) {
				if(customers[i] == null) continue;
				for (int j=0;j<customers[i].getCustomerAccountsList().length;j++) {
					if(customers[i].getCustomerAccountsList()[j] == null) continue;
					System.out.println(customers[i].getCustomerAccountsList()[j]);
				}
			}
		}
//#######################################################################################
// Menu Finances
	
		private static void menuFinances() {
			
			if(isLogged) {
				do {
					System.out.printf("1 - Deposit%n"
									+ "2 - Withdraw%n"
									+ "3 - Transfer%n"
									+ "4 - Show my accounts%n"
									+ "5 - Add Secondary Holder%n"
									+ "6 - Delete Secondary Holder%n"
									+ "7 - Manage Accounts%n"
									+ "8 - Create new Account%n"
									+ "9 - Delete account by Acc ID%n"
									+ "0 - Exit%n");
					input = sc.nextInt();
					
					switch(input) {
					
						case 0:
							menuHome();
							break;
						case 1:	
							deposit();
							break;
						case 2:
							withdraw();
							break;
						case 3:
							transferByAccountId();
							break;
						case 4:
							showMyAccounts();
							break;
						case 5:
							addSecondaryAccountHoldersByTaxId();
							break;
						case 6:
							deleteSecondaryAccountHoldersByTaxId();
							break;
						case 7:
							menuManageAccounts();
							break;
						case 8:
							addNewAccount();
							break;
						case 9:
							deleteAccountByAccountId();
							break;
							
						default:
							break;
					}
				} while (input!=0);
			}
		}
		
			private static void login() {
				System.out.print("Please insert your name: ");
				String username = sc.next();
				System.out.print("Please insert your password: ");
				String password = sc.next();
				
				for(int i=0;i<customers.length;i++) {
					if(customers[i] == null) continue;
					if(customers[i].getName().equals(username) && customers[i].getPassword().equals(password)) {
						loggedUser = customers[i];
						System.out.println("Login successful");
						isLogged = true;
						return;
					}
				}
				System.err.println("Login credencials incorrect.");
			}
			
			private static void deposit() {
				System.out.print("How much do you want to deposit?: �");
				Double deposit = sc.nextDouble();
				loggedUser.getAccount().setBalance(loggedUser.getAccount().getBalance()+deposit);
				System.out.printf("Account balance: %.2f�%n",loggedUser.getAccount().getBalance());
			}
			
			private static void withdraw() {
				System.out.print("How much you want to withdraw?: �");
				Double withdraw = sc.nextDouble();
				if(loggedUser.getAccount().getBalance()-withdraw>=0){
					loggedUser.getAccount().setBalance(loggedUser.getAccount().getBalance()-withdraw);
					System.out.printf("Account balance: %.2f�%n",loggedUser.getAccount().getBalance());
					return;
				}
				else
					System.err.println("Not enough capital in account");
			}
			
			private static void transferByAccountId() {
				
// Check if Balance in my acc is OK // Set balance in acc id (balance+amount)		
				
				System.out.print("Enter account ID: ");
				input = sc.nextInt();
				System.out.print("Enter amount to transfer: �");
				Double amount = sc.nextDouble();
				
				if(loggedUser.getAccount().getBalance()-amount<0)
					System.err.println("Not enough capital in account");			
				for (int i=0; i<customers.length;i++) {
					if(customers[i]== null) continue;
					for(int j=0;j<customers[i].getCustomerAccountsList().length;j++) {
						if(customers[i].getCustomerAccountsList()[j]==null) continue;
						if(customers[i].getCustomerAccountsList()[j].getAccountId() == input) {
							customers[i].getCustomerAccountsList()[j].setBalance(customers[i].getCustomerAccountsList()[j].getBalance()+amount);
							loggedUser.getAccount().setBalance(loggedUser.getAccount().getBalance()-amount);
							System.out.printf("Account balance:%.2f%n",loggedUser.getAccount().getBalance());
//							return;
						}
					}
				}
			} 
			
			private static void showMyAccounts() {
				
				for (int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
					if(loggedUser.getCustomerAccountsList()[i] == null) continue;
					System.out.println(loggedUser.getCustomerAccountsList()[i]);
				}
			}
			
			private static String returnTaxId() {
				System.out.print("Enter tax Id: ");
				String taxId = sc.next();
				String sendTaxId = "ND";
				
				for (int i=0;i<customers.length;i++) {
					if(customers[i] ==null) continue;
					if(customers[i].getTaxId().equals(taxId))
						sendTaxId = taxId;
					}
				return sendTaxId;
			}
			
			private static Customer returnCustomerByTaxId(String sendTaxId) {
				
				Customer x = null;
				// checkCustomerListHasNullSlot()
				for (int i=0; i<customers.length; i++) {
					if (customers[i]==null)	continue;
					if(sendTaxId.equals(customers[i].getTaxId()))
						x = customers[i];
				}
				return x;
			}
			
			private static boolean isDuplicatedTaxId(String sendTaxId) {
				boolean isDuplicated=false;
				for(int i=0;i<loggedUser.getAccount().getSecondaryAccountHolders().length;i++) {
					if(loggedUser.getAccount().getSecondaryAccountHolders()[i]==null) continue;
					if(loggedUser.getAccount().getSecondaryAccountHolders()[i].getTaxId().equals(sendTaxId)) {
						System.err.println("This Tax ID already is a secondary account holder");
						isDuplicated=true;
					}
				}
				return isDuplicated;
			}
			
			private static void addSecondaryAccountHoldersByTaxId() {
			
				String taxId=returnTaxId();
				Customer cust = returnCustomerByTaxId(taxId);
				
				if(!isDuplicatedTaxId(taxId) && !taxId.equals("ND")) {
					for(int i=0;i<loggedUser.getAccount().getSecondaryAccountHolders().length;i++) {
						if(loggedUser.getAccount().getSecondaryAccountHolders()[i]==null) {
							loggedUser.getAccount().getSecondaryAccountHolders()[i]=cust;
							return;
						}
					}
					System.err.println("This Account can't have more holders");
				}
			}
			
			private static void deleteSecondaryAccountHoldersByTaxId() {
				String taxId=returnTaxId();
				
					for(int i=0;i<loggedUser.getAccount().getSecondaryAccountHolders().length;i++) {
						if(loggedUser.getAccount().getSecondaryAccountHolders()[i].getTaxId().equals(taxId)) {
							loggedUser.getAccount().getSecondaryAccountHolders()[i]=null;
							System.out.println("Secondary account holder deleted");
							return;
						}
					}
					System.out.println("Tax ID Not found");
				}
		
			private static boolean isCustomerAccountListFree() {
				
				for(int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
					if(loggedUser.getCustomerAccountsList()[i] == null) continue;
				}
				return true;
			}
	
			private static DebitCard createDebitCard() {
				
				DebitCard newDebitCard = new DebitCard();
				return newDebitCard;
			}
			
			private static CreditCard createCreditCard() {
				
				CreditCard newCreditCard = new CreditCard();
				System.out.print("Define Credit Card plafond: ");
				Double plafond = sc.nextDouble();
				newCreditCard.setPlafondBalance(plafond);
				
				return newCreditCard;
			}
			
			private static void addNewAccount() {
				
				if(isCustomerAccountListFree()) {
					System.out.print("Insert initial deposit: ");
					Double balance = sc.nextDouble();
					
					Account newAccount = new Account(loggedUser.getAccount().getAccountHolder(),balance);
					
					newAccount.setDebitCard(createDebitCard());
					newAccount.setCreditCard(createCreditCard());
					
					for(int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
						if(loggedUser.getCustomerAccountsList()[i] == null) {
							loggedUser.getCustomerAccountsList()[i] = newAccount;
							return;
						}
					}
				}
				System.err.println("Can't have more accounts");
			}
			
			private static void deleteAccountByAccountId() {
				
				showMyAccounts();
				System.out.print("Which account to do you want to delete? ");
				input = sc.nextInt();
				for(int i=0; i<loggedUser.getCustomerAccountsList().length;i++) {
					if(loggedUser.getCustomerAccountsList()[i] == null) continue;
					if(loggedUser.getCustomerAccountsList()[i].getAccountId() == input) {
						loggedUser.getCustomerAccountsList()[i] = null;
						return;
					}
				}
				System.err.println("Account ID not found");
				
			}

		private static Account chooseAccountId() {
					
			showMyAccounts();
			System.out.print("Choose Account ID: ");
			int id = sc.nextInt();
			Account account = null;
			
			for(int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
				if(loggedUser.getCustomerAccountsList()[i] == null) continue;
				if(loggedUser.getCustomerAccountsList()[i].getAccountId() == id)
					account = loggedUser.getCustomerAccountsList()[i];
			}
			return account;
		}
		
		private static void menuManageAccounts() {
			
				Account account = chooseAccountId();	

// SE O CLIENTE FALHAR O ID DA CONTA ELE PASSA PARA O MENU ABAIXO COM UMA ACC null // REVER ESTA PARTE
			do {
				System.out.printf("1 - Show account info%n"
								+ "2 - Add Secondary Holder%n"
								+ "3 - Delete Secondary Holder%n"
								+ "4 - Manage Cards%n"
								+ "0 - Exit%n");
				
				input = sc.nextInt();
				switch(input) {
				
					case 0:
						menuFinances();
						break;
					case 1:
						showAccountInfo(account);
						break;
					case 2:
						addSecondaryAccountHoldersByAccountId(account);
						break;
					case 3:
						deleteSecondaryAccountHoldersByAccountId(account);
						break;
					case 4:
						menuCards(account);
						break;
					default:
						break;
				}
					
			} while(input!=0);
		}	
		
// Para este menu vamos enviar sempre a account como parametro
		private static void showAccountInfo(Account account) {
			for(int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
				if(loggedUser.getCustomerAccountsList()[i] == account)
					System.out.println(loggedUser.getCustomerAccountsList()[i]);
			}
		}
		
		private static void addSecondaryAccountHoldersByAccountId(Account account) {
			
			String taxId=returnTaxId();
			Customer cust = returnCustomerByTaxId(taxId);
			
			if(!isDuplicatedTaxId(taxId) && !taxId.equals("ND")) {
				for(int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
					if(loggedUser.getCustomerAccountsList()[i] == null) continue;
					if(loggedUser.getCustomerAccountsList()[i] == account) {
						for(int j=0;j<loggedUser.getCustomerAccountsList()[i].getSecondaryAccountHolders().length;j++) {
							if(loggedUser.getCustomerAccountsList()[i].getSecondaryAccountHolders()[j] != null) 
								continue;
							else
								loggedUser.getCustomerAccountsList()[i].getSecondaryAccountHolders()[j] = cust;
								return;
					}
						System.err.println("This Account can't have more holders");

				}
			}
		}
	}
		
		private static void deleteSecondaryAccountHoldersByAccountId(Account account) {
			String taxId=returnTaxId();
			Customer cust = returnCustomerByTaxId(taxId);
			
			if(!isDuplicatedTaxId(taxId) && !taxId.equals("ND")) {
				for(int i=0;i<loggedUser.getCustomerAccountsList().length;i++) {
					if(loggedUser.getCustomerAccountsList()[i] == null) continue;
					if(loggedUser.getCustomerAccountsList()[i] == account) {
						for(int j=0;j<loggedUser.getCustomerAccountsList()[i].getSecondaryAccountHolders().length;j++) {
							if(loggedUser.getCustomerAccountsList()[i].getSecondaryAccountHolders()[j] == cust) {
								loggedUser.getCustomerAccountsList()[i].getSecondaryAccountHolders()[j] = null;
								return;
							}
						}
						System.err.println("Customer not found.");

				}
			}
		}
	}
//#######################################################################################
// Menu Cartoes
			
		private static void menuCards(Account account) {
				
			do {
				System.out.printf("1 - Show all account cards%n"
								+ "2 - Define Credit Card Plafond%n"
								+ "3 - Cancel Debit Card%n"
								+ "4 - Cancel Credit Card%n"
								+ "5 - Request Debit Card%n"
								+ "6 - Request Credit Card%n"
								+ "0 - Exit%n");
					
				input = sc.nextInt();
				switch (input) {
				
					case 0:
						menuFinances();
						break;
					case 1:
						//Show all account cards
						break;
					case 2:
						//Define Credit Card Plafond
						break;
					case 3:
						//Cancel Debit Card
						break;
					case 4:
						//Cancel Credit Card%n
						break;
					case 5:
						//Request Debit Card
						break;
					case 6:
						//Request Credit Card
						break;
					default:
						break;	
					}
				} while (input!=0);
			}

//####################################################################################
		private static Customer[] fillDB() {
			Customer[] people = new Customer[5];
			
			Customer c1 = new Customer("Joaquim","j1980","1","Joaquim@gmail.com");
			Account a1 = new Account(c1,450.00);
			c1.setAccount(a1);
			c1.getCustomerAccountsList()[0] = a1;
			
			DebitCard dc1 = new DebitCard();
			CreditCard cc1 = new CreditCard();
			a1.setDebitCard(dc1);
			a1.setCreditCard(cc1);
			
			Customer c2 = new Customer("Ana","A1999","2","Ana@gmail.com");
			Account a2 = new Account(c2,2000.00);
			c2.setAccount(a2);
			c2.getCustomerAccountsList()[0] = a2;
			
			DebitCard dc2 = new DebitCard();
			CreditCard cc2 = new CreditCard();
			a2.setDebitCard(dc2);
			a2.setCreditCard(cc2);
			
			Customer c3 = new Customer("Jose","J1976","3","Jose@gmail.com");
			Account a3 = new Account(c3,1500.00);
			c3.setAccount(a3);
			c3.getCustomerAccountsList()[0] = a3;
			DebitCard dc3 = new DebitCard();
			CreditCard cc3 = new CreditCard();
			a3.setDebitCard(dc3);
			a3.setCreditCard(cc3);
			
			Customer c4 = new Customer("Maria","M2003","4","Maria@gmail.com");
			Account a4 = new Account(c4,850.00);
			c4.setAccount(a4);
			c4.getCustomerAccountsList()[0] = a4;
			DebitCard dc4 = new DebitCard();
			CreditCard cc4 = new CreditCard();
			a4.setDebitCard(dc4);
			a4.setCreditCard(cc4);
			
			Customer c5 = new Customer("Manny","xpto","5","Manny@CrazyBoyo.com");
			Account a5 = new Account(c5,5000.00);
			c5.setAccount(a5);
			c5.getCustomerAccountsList()[0] = a5;
			DebitCard dc5 = new DebitCard();
			CreditCard cc5 = new CreditCard();
			a5.setDebitCard(dc5);
			a5.setCreditCard(cc5);
			
			
			people[0] = c1;
			people[1] = c2;
			people[2] = c3;
			people[3] = c4;
			people[4] = c5;

			int j=0;
			for (int i=0;i<people.length;i++) {
				customers[i] = people[j];
				j++;
			}
			return customers;
		}
}
//####################################################################################		
//DRY - method to be used	// going through null slots 
//private static void checkCustomerListHasNullSlot() {
//	for (int i=0; i<customers.length; i++) {
//		if (customers[i]==null)
//			continue;
//	}
//}




		