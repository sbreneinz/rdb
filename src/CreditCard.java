
public class CreditCard extends Cards {
	
	private double plafondBalance;
	private final int maxCreditCardNum = 2;
	private final int firstUse = 1;
	private final String defaultCreditCardNumber = "66667777";
	private String creditCardNumber;
	private static Integer counter=1;
	
	public CreditCard() {
		CreditCardNumberGenerator();
	}
	protected void CreditCardNumberGenerator() {
		creditCardNumber=defaultCreditCardNumber+counter;
		counter++;
	}
	protected double getPlafondBalance() {
		return plafondBalance;
	}
	protected void setPlafondBalance(double plafondBalance) {
		this.plafondBalance = plafondBalance;
	}
	@Override
	public String toString() {
		return "[plafondBalance=" + plafondBalance + ", creditCardNumber=" + creditCardNumber + "]";
	}

	

	// setAutomaticPin() {
	//}
}
