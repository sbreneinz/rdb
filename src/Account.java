import java.util.Arrays;

public class Account {
	
	private Customer accountHolder;
	private int accountId;
	private static int counter;
	private Customer[] secondaryAccountHolders = new Customer[3];
	private CreditCard creditCard;
	private DebitCard debitCard;
	private Double balance;
	
	
// constructor for fillDB();	
	protected Account(Customer newCustomer, Double balance) {
		this.accountHolder = newCustomer;
		this.balance = balance;
		calculateAccountId();
	}

	protected int getAccountId() {
		return accountId;
	}
	protected int setAccountId(int counter) {
		this.accountId = counter;
		return accountId;
	}
	protected void calculateAccountId() {
		counter++;
		setAccountId(counter);
	}
	protected Customer getAccountHolder() {
		return accountHolder;
	}
	protected void setAccountHolder(Customer accountHolder) {
		this.accountHolder = accountHolder;
	}
	protected Customer[] getSecondaryAccountHolders() {
		return secondaryAccountHolders;
	}
	protected void setSecondaryAccountHolders(Customer[] secondaryAccountHolders) {
		this.secondaryAccountHolders = secondaryAccountHolders;
	}
	protected CreditCard getCreditCard() {
		return creditCard;
	}
	protected void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	protected DebitCard getDebitCard() {
		return debitCard;
	}
	protected void setDebitCard(DebitCard debitCard) {
		this.debitCard = debitCard;
	}
	protected Double getBalance() {
		return balance;
	}
	protected void setBalance(Double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [accountHolder=" + accountHolder + ", accountId=" + accountId + ", secondaryAccountHolders="
				+ Arrays.toString(secondaryAccountHolders) + ", creditCard=" + creditCard + ", debitCard=" + debitCard
				+ ", balance=" + balance + "]";
	}
	


	

}